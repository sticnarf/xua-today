#[macro_use]
extern crate cqpsdk;
#[macro_use]
extern crate lazy_static;
extern crate encoding;

use cqpsdk::{Client, LogLevel};
use encoding::all::GB18030;
use encoding::{DecoderTrap, Encoding};
use std::ffi::{CStr, CString};
use std::os::raw::c_char;
use std::sync::RwLock;

lazy_static! {
    static ref CLIENT: RwLock<Client<'static>> = RwLock::new(Client::new("me.sticnarf.xuatoday"));
    static ref APP_INFO: CString = {
        let client = CLIENT.read().unwrap();
        CString::new(client.app_info()).unwrap()
    };
}

#[export_name = "AppInfo"]
pub extern "stdcall" fn app_info() -> *const c_char {
    APP_INFO.as_ptr()
}

#[export_name = "Initialize"]
pub extern "stdcall" fn initialize(auth_code: i32) -> i32 {
    let mut client = CLIENT.write().unwrap();
    client.initialize(auth_code);
    0
}

#[export_name = "CQPStartupHandler"]
pub extern "stdcall" fn cqp_startup_handler() -> i32 {
    0
}

#[export_name = "CQPExitHandler"]
pub extern "stdcall" fn cqp_exit_handler() -> i32 {
    0
}

#[export_name = "EnableHandler"]
pub extern "stdcall" fn cqp_enable_handler() -> i32 {
    let client = CLIENT.read().unwrap();
    client.add_log(LogLevel::Info, "what", "the fuck!");
    0
}

#[export_name = "DisableHandler"]
pub extern "stdcall" fn cqp_disable_handler() -> i32 {
    let client = CLIENT.read().unwrap();
    client.add_log(LogLevel::Info, "fuck", "you!");
    0
}

#[export_name = "PrivateMessageHandler"]
pub extern "stdcall" fn private_message_handler(
    sub_type: i32,
    msg_id: i32,
    qq_num: i64,
    msg: *const c_char,
    font: i32,
) -> i32 {
    let msg = unsafe { GB18030_C_CHAR_PRT_TO_UTF8_STR!(msg) };

    let client = CLIENT.read().unwrap();
    // Private message subtype: 11
    let log = format!(
        "Subtype: {}, Msgid: {}, QQ: {}, msg: {}, font: {}",
        sub_type, msg_id, qq_num, msg, font
    );
    client.add_log(LogLevel::Info, "PM", &log);
    client.send_private_message(qq_num, msg);
    return cqpsdk::EVENT_BLOCK;
}
